/*
 * 
 * GoogleMap components
 * This code is property of Ilya V. Azarov, (c) 2008
 * You can use it under terms of GPL
 * 
 */

Drupal.googlemap = Drupal.googlemap || {};
Drupal.googlemap.markers = Drupal.googlemap.markers || {};
Drupal.googlemap.markers_num = Drupal.googlemap.markers_num || 0;
$(document).ready(function(){
    if (GBrowserIsCompatible()) {
        Drupal.googlemap.changed = false;
        Drupal.googlemap.changedMessage(Drupal.googlemap.changed);
        Drupal.googlemap.map = new GMap2(document.getElementById("googlemap"));
        Drupal.googlemap.map.addControl(new GLargeMapControl());
        Drupal.googlemap.map.addControl(new GScaleControl() );
        Drupal.googlemap.map.setCenter(new GLatLng(Drupal.googlemap.settings.lat, Drupal.googlemap.settings.lng) );
        Drupal.googlemap.markerManager = new MarkerManager(Drupal.googlemap.map, {});
        if(Drupal.googlemap.markers_num > 0){
            i = 0;
            if(Drupal.googlemap.markers_num == 1){
                i = 0;
                Drupal.googlemap.map.setCenter(new GLatLng(Drupal.googlemap.markers[i].lat, Drupal.googlemap.markers[i].lng) );
                Drupal.googlemap.map.setZoom(16);
            }else{
                i = 0;
                minlat = Drupal.googlemap.markers[i].lat;
                minlng = Drupal.googlemap.markers[i].lng;
                maxlat = Drupal.googlemap.markers[i].lat;
                maxlng = Drupal.googlemap.markers[i].lng;
                for(i = 0; i < Drupal.googlemap.markers_num; i ++){
                    if(minlat < Drupal.googlemap.markers[i].lat) minlat = Drupal.googlemap.markers[i].lat;
                    if(minlng < Drupal.googlemap.markers[i].lng) minlng = Drupal.googlemap.markers[i].lng;
                    if(maxlat > Drupal.googlemap.markers[i].lat) maxlat = Drupal.googlemap.markers[i].lat;
                    if(maxlng > Drupal.googlemap.markers[i].lng) maxlng = Drupal.googlemap.markers[i].lng;
                }
                i = 0;
                if(minlat == maxlat || minlng == maxlng) 
                    Drupal.googlemap.map.setCenter(new GLatLng(Drupal.googlemap.markers[i].lat, Drupal.googlemap.markers[i].lng) );
                else{
                    clat = (minlat + maxlat)/2;
                    clng = (minlng + maxlng)/2;
                    //Drupal.googlemap.map.setCenter(clat, clng);
                    //alert('хуй');
                    //ml = new GLatLng(minlat, maxlng);
                    gm = new GLatLngBounds(new GLatLng(minlat, maxlng), new GLatLng(maxlat, minlng) );
                    Drupal.googlemap.map.setCenter(gm.getCenter() );
                    z = Drupal.googlemap.map.getBoundsZoomLevel(gm);
                    Drupal.googlemap.map.setZoom(z);
                }
            }
            for(i = 0; i < Drupal.googlemap.markers_num; i ++){
                marker = new GMarker(
                    new GLatLng(Drupal.googlemap.markers[i].lat, Drupal.googlemap.markers[i].lng ), 
                    {title: 
                        ( (typeof(Drupal.googlemap.node_values.title) != 'undefined') ? Drupal.googlemap.node_values.title : '' ) 
                        + ' (' + Drupal.googlemap.settings.lang_11 + ')'
                    }
                );
                if((typeof(Drupal.googlemap.node_values.title) != 'undefined') ){
                    marker.bindInfoWindowHtml(
                        '<div style="display:block;margin-top:10px;padding-bottom:20px;overflow:auto;padding-right:20px;width:350px;height:200px">' + Drupal.googlemap.node_teaser + '</div>'
                    );
                }
                Drupal.googlemap.markerManager.addMarker(marker, 8, 17);
            }
            Drupal.googlemap.markerManager.refresh();
        }else{
            Drupal.googlemap.map.setCenter(new GLatLng(Drupal.googlemap.settings.lat, Drupal.googlemap.settings.lng) );
            Drupal.googlemap.map.setZoom(12);   
        }
        Drupal.googlemap.map.enableScrollWheelZoom();
        $('#addmarker').click(function(){
            alert(Drupal.googlemap.settings.lang_2);
            Drupal.googlemap.removeOldClickHandler();
            Drupal.googlemap.OldClickHandler = GEvent.addListener(
                Drupal.googlemap.map,
                "click", Drupal.googlemap.mapAddMarkerClick
            );
        });
        $('#removemarker').click(function(){
            alert(Drupal.googlemap.settings.lang_1);
            Drupal.googlemap.removeOldClickHandler();
            Drupal.googlemap.OldClickHandler = GEvent.addListener(
                Drupal.googlemap.map,
                "click", Drupal.googlemap.mapRemoveMarkerClick
            );
        });
        $('#savemarkers').click(function(){
            Drupal.googlemap.saveLocations();
        });
    }else{
        // add an alert here
        alert(Drupal.googlemap.settings.lang_10);
    }
});
$(document).unload(function(){
    GUnload();
});
Drupal.googlemap.removeOldClickHandler = function(){
    if(Drupal.googlemap.OldClickHandler) GEvent.removeListener(Drupal.googlemap.OldClickHandler);
    Drupal.googlemap.OldClickHandler = false;
}
Drupal.googlemap.mapAddMarkerClick = function(marker, point){
    if(marker){
        alert(Drupal.googlemap.settings.lang_0);
    }else{
        if(Drupal.googlemap.markers_num <= 50){
            marker = new GMarker(new GLatLng(point.lat(), point.lng() ), {title: ''});
            Drupal.googlemap.map.addOverlay(marker);
            Drupal.googlemap.markers[Drupal.googlemap.markers_num] = {lat:point.lat(), lng:point.lng()};
            Drupal.googlemap.markers_num ++;
            Drupal.googlemap.changed = true;
        }else{
            alert(Drupal.googlemap.settings.lang_6);
        }
    }
    Drupal.googlemap.removeOldClickHandler();
    Drupal.googlemap.changedMessage(Drupal.googlemap.changed);
}

Drupal.googlemap.mapRemoveMarkerClick = function(marker, point){
    if(typeof(marker) != "undefined"){
        latlng = marker.getLatLng();
        marker.remove();
        markers = {};
        markers_num = 0;
        for(i = 0; i < Drupal.googlemap.markers_num; i++){
            if( (Drupal.googlemap.markers[i].lat != latlng.lat() ) || (Drupal.googlemap.markers[i].lng != latlng.lng() ) ){
                markers[markers_num] = Drupal.googlemap.markers[i];
                markers_num ++;
            }
        }
        Drupal.googlemap.markers = markers;
        Drupal.googlemap.markers_num = markers_num;
        Drupal.googlemap.changed = true;
    }else{
        alert(Drupal.googlemap.settings.lang_3);
    }
    Drupal.googlemap.removeOldClickHandler();
    Drupal.googlemap.changedMessage(Drupal.googlemap.changed);
}
Drupal.googlemap.changedMessage = function(changed){
    $("#changedmessage").html(changed ? Drupal.googlemap.settings.lang_7 : "");
    if(changed) $('#savemarkers').removeAttr("disabled");
    else $('#savemarkers').attr("disabled", "true");
}
Drupal.googlemap.saveLocations = function(){
    //we'll make stupid string
    var s = '';
    for(i = 0; i < Drupal.googlemap.markers_num; i++){
        s = s + Drupal.googlemap.markers[i].lat + ':' + Drupal.googlemap.markers[i].lng;
        if(i != Drupal.googlemap.markers_num - 1) s = s + '#';
    }
    $.ajax({
        type: "POST", 
        url: Drupal.googlemap.settings.jsonurl,
        dataType : 'json',
        data: {st: s, form_token: Drupal.googlemap.form_token},
        success:  function(msg){
            if(typeof(msg.success) == "undefined"){ 
                if(typeof(msg.error) != "undefined") alert(msg.error);
                else alert(Drupal.googlemap.settings.lang_9);
            }else{
                Drupal.googlemap.changed = false;
                Drupal.googlemap.changedMessage(Drupal.googlemap.changed);
                alert(msg.success);
            }
        },
        error: function(request, err, except){
            alert(Drupal.googlemap.settings.lang_8 + err);
        }
    });
}
