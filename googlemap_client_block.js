/*
 * 
 * GoogleMap components
 * This code is property of Ilya V. Azarov, (c) 2008
 * You can use it under terms of GPL
 * 
 */

Drupal.googlemap = Drupal.googlemap || {};
Drupal.googlemap.markers = Drupal.googlemap.markers || {};
Drupal.googlemap.markers_num = Drupal.googlemap.markers_num || 0;
// SPECIAL THANK YOU GREGORY MANUSHKIN, YOU'VE DONE ABOUT NOTHING AND WASTED MY TIME
$(document).ready(function(){
    if (GBrowserIsCompatible()) {
        Drupal.googlemap.changed = false;
        //Drupal.googlemap.changedMessage(Drupal.googlemap.changed);
        Drupal.googlemap.map = new GMap2(document.getElementById("googlemap_block"));
        //Drupal.googlemap.map.addControl(new GLargeMapControl());
        //Drupal.googlemap.map.addControl(new GScaleControl() );
        Drupal.googlemap.map.setCenter(new GLatLng(Drupal.googlemap.settings.lat, Drupal.googlemap.settings.lng) );
        Drupal.googlemap.markerManager = new MarkerManager(Drupal.googlemap.map, {});
        if(Drupal.googlemap.markers_num > 0){
            i = 0;
            if(Drupal.googlemap.markers_num == 1){
                i = 0;
                Drupal.googlemap.map.setCenter(new GLatLng(Drupal.googlemap.markers[i].lat, Drupal.googlemap.markers[i].lng) );
                Drupal.googlemap.map.setZoom(16);
            }else{
                i = 0;
                minlat = Drupal.googlemap.markers[i].lat;
                minlng = Drupal.googlemap.markers[i].lng;
                maxlat = Drupal.googlemap.markers[i].lat;
                maxlng = Drupal.googlemap.markers[i].lng;
                for(i = 0; i < Drupal.googlemap.markers_num; i ++){
                    if(minlat < Drupal.googlemap.markers[i].lat) minlat = Drupal.googlemap.markers[i].lat;
                    if(minlng < Drupal.googlemap.markers[i].lng) minlng = Drupal.googlemap.markers[i].lng;
                    if(maxlat > Drupal.googlemap.markers[i].lat) maxlat = Drupal.googlemap.markers[i].lat;
                    if(maxlng > Drupal.googlemap.markers[i].lng) maxlng = Drupal.googlemap.markers[i].lng;
                }
                i = 0;
                if(minlat == maxlat || minlng == maxlng) 
                    Drupal.googlemap.map.setCenter(new GLatLng(Drupal.googlemap.markers[i].lat, Drupal.googlemap.markers[i].lng) );
                else{
                    clat = (minlat + maxlat)/2;
                    clng = (minlng + maxlng)/2;
                    gm = new GLatLngBounds(new GLatLng(minlat, maxlng), new GLatLng(maxlat, minlng) );
                    Drupal.googlemap.map.setCenter(gm.getCenter() );
                    z = Drupal.googlemap.map.getBoundsZoomLevel(gm);
                    Drupal.googlemap.map.setZoom(z);
                }
            }
            for(i = 0; i < Drupal.googlemap.markers_num; i ++){
                marker = new GMarker(
                    new GLatLng(Drupal.googlemap.markers[i].lat, Drupal.googlemap.markers[i].lng ), 
                    {title: 
                        (typeof(Drupal.googlemap.node_values.title) != 'undefined') 
                            ? Drupal.googlemap.node_values.title : ''
                    }
                );
                Drupal.googlemap.markerManager.addMarker(marker, 8, 17);
            }
            Drupal.googlemap.markerManager.refresh();
            //Drupal.googlemap.refresh();
        }else{
            Drupal.googlemap.map.setCenter(new GLatLng(Drupal.googlemap.settings.lat, Drupal.googlemap.settings.lng) );
            Drupal.googlemap.map.setZoom(12);   
        }
        Drupal.googlemap.map.enableScrollWheelZoom();
    }else{
        alert(Drupal.googlemap.settings.lang_10);
    }
});
$(document).unload(function(){
    GUnload();
});
